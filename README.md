# Hack.programando.li/breros

Repositorio del sitio de Hackedición.

# Licencia

Este _fanzine_ tiene [Licencia Editorial Abierta y Librea (LEAL)](https://programando.li/bres/).

Con LEAL eres libre de usar, copiar, reeditar, modificar, distribuir
o comercializar bajo las siguientes condiciones:

* Los productos derivados o modificados han de heredar algún tipo de LEAL.
* Los archivos editables y finales habrán de ser de acceso público.
* El contenido no puede implicar difamación, explotación o vigilancia.
